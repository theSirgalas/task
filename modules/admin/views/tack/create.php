<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\entities\Tack */

$this->title = 'Create Tack';
$this->params['breadcrumbs'][] = ['label' => 'Tacks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tack-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'user'  => $user
        
    ]) ?>

</div>
