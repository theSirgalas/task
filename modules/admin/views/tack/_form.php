<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model app\entities\Tack */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="tack-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'data_start')->widget(DateTimePicker::class,[
        'type' => DateTimePicker::TYPE_INPUT,
        'value' => '2018-04-29 10-10-00',
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'yyyy-mm-dd hh-ii-ss'
        ]
    ]); ?>

    <?= $form->field($model, 'data_finish')->widget(DateTimePicker::class,[
        'type' => DateTimePicker::TYPE_INPUT,
        'value' => '2018-04-29 10-10-00',
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'yyyy-mm-dd hh-ii-ss'
        ]
    ]); ?>

    <?= $form->field($model, 'users_list')->widget(Select2::class, [
        'data' => $user,
        'language' => 'en',
        'options' => ['multiple' => true,'placeholder' => 'Select a state ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
