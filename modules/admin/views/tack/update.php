<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\entities\Tack */

$this->title = 'Update Tack: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Tacks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tack-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'user'  => $user
    ]) ?>

</div>
