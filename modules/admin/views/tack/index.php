<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel app\search\TackSearch */
/* @var $dataProvider yii\data\ActiveDataProvider
 * @var $model app\entities\User */

$this->title = 'Tacks';
$this->params['breadcrumbs'][] = $this->title;
$user_columns=[
    ['class' => 'yii\grid\SerialColumn'],
    'id',
    'name',
    'price',
    'data_start',
    'data_finish',
];
if(Yii::$app->user->identity->isAdmin()){
    $user_columns[]=[
        'attribute'=> 'user',
        'format'=>'raw',
        'value'=>function($model){
            $result='';
            if(!empty($model->users)) {
                foreach ($model->users as $user) {
                    $result .= "<p> login " . $user->login.' first name '.$user->first_name.' last name '.$user->last_name.'</p>';
                }
            }
            return $result;
        },
        'filter' => Select2::widget([
            'name' => 'TackSearch[parent]',
            'data' => $user,
            'size' => Select2::MEDIUM,
            'options' => ['placeholder' => 'Select a second name'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])
    ];
    $user_columns[]=[
        'class' => 'yii\grid\ActionColumn'
    ];
}

?>
<div class="tack-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if(Yii::$app->user->identity->isAdmin()){ ?>
        <p>
            <?= Html::a('Create Tack', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php } ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $user_columns,
    ]); ?>
</div>
