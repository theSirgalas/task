<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 29.04.18
 * Time: 15:40
 */

namespace app\services;

use app\forms\SignupForm;
use app\entities\User;

class SignupService
{

    public function signup(SignupForm $form): User
    {
        if(User::find()->andWhere(['login'=>$form->login])->one()){
            throw new \DomainException('Login is already exist');
        }
        if(User::find()->where(['first_name'=>$form->first_name])->andWhere(['last_name'=>$form->last_name])->one()){
            throw new \DomainException('Name  already exist');
        }
        $user = User::signup(
            $form->login,
            $form->first_name,
            $form->last_name,
            $form->password
        );
        if (!$user->save()) {
            throw new \RuntimeException('Saving Error');
        }
        return $user;
    }
}