<?php

namespace app\search;

use app\entities\User;
use app\entities\UserTack;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\entities\Tack;

/**
 * TackSearch represents the model behind the search form of `app\entities\Tack`.
 */
class TackSearch extends Tack
{
    public $parent;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'price','parent'], 'integer'],
            [['name', 'data_start', 'data_finish'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tack::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        if(Yii::$app->user->identity->isAdmin()){
            $userTacks=UserTack::find()->all();
            foreach ($userTacks as $userTack){
                $idsTack[]=$userTack->tack_id;
            }
        }else{
            foreach (Yii::$app->user->identity->userTacks as $userTack)
                $idsTack[]=$userTack->tack_id;
        }
        
        if($this->parent){
            $idsTack=array();
            $usersTack=UserTack::find()->where(['user_id'=>(int)$this->parent])->all();
            foreach ($usersTack as $userTack){
                $idsTack[]=$userTack->tack_id;
            }
        }
        
        if(empty($idsTack))
            $idsTack=0;

        $query->andFilterWhere([
            'id' => $this->id,
            'price' => $this->price,
            'data_start' => $this->data_start,
            'data_finish' => $this->data_finish,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        $query->andFilterWhere(['in', 'id', $idsTack]);

        return $dataProvider;
    }
}
