<?php

namespace app\entities;

use Yii;

/**
 * This is the model class for table "user_tack".
 *
 * @property int $user_id
 * @property int $tack_id
 *
 * @property Tack $tack
 * @property User $user
 */
class UserTack extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_tack';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'tack_id'], 'required'],
            [['user_id', 'tack_id'], 'integer'],
            [['user_id', 'tack_id'], 'unique', 'targetAttribute' => ['user_id', 'tack_id']],
            [['tack_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tack::className(), 'targetAttribute' => ['tack_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'tack_id' => 'Tack ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTack()
    {
        return $this->hasOne(Tack::className(), ['id' => 'tack_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
