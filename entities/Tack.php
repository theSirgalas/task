<?php

namespace app\entities;

use Yii;
use app\components\behaviors\ManyHasManyBehavior;

/**
 * This is the model class for table "tack".
 *
 * @property int $id
 * @property string $name
 * @property int $price
 * @property string $data_start
 * @property string $data_finish
 *
 * @property UserTack[] $userTacks
 * @property User[] $users
 */
class Tack extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tack';
    }

    public function behaviors()
    {
        return [
            'user'=>[
                'class' => ManyHasManyBehavior::class,
                'relations' => [
                    'users' => 'users_list',
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'price', 'data_start', 'data_finish'], 'required'],
            [['price'], 'integer'],
            [['data_start', 'data_finish','users_list'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'price' => 'Price',
            'data_start' => 'Data Start',
            'data_finish' => 'Data Finish',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserTacks()
    {
        return $this->hasMany(UserTack::class, ['tack_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::class, ['id' => 'user_id'])->viaTable('user_tack', ['tack_id' => 'id']);
    }
}
