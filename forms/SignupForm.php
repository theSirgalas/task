<?php
/**
 * @property string $username
 * @property string $email
 * @property string $password
 */

namespace app\forms;
use Yii;
use yii\base\Model;
use app\entities\User;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class SignupForm extends Model
{
    public $login;
    public $first_name;
    public $last_name;
    public $password;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['login', 'trim'],
            ['login', 'required'],
            ['login', 'unique', 'targetClass' => '\app\entities\User', 'message' => 'This username has already been taken.'],
            ['login', 'string', 'min' => 2, 'max' => 255],
            ['first_name', 'trim'],
            ['first_name', 'required'],
            ['first_name', 'unique', 'targetClass' => '\app\entities\User', 'message' => 'This username has already been taken.'],
            ['first_name', 'string', 'min' => 2, 'max' => 255],
            ['last_name', 'trim'],
            ['last_name', 'required'],
            ['last_name', 'unique', 'targetClass' => '\app\entities\User', 'message' => 'This username has already been taken.'],
            ['last_name', 'string', 'min' => 2, 'max' => 255],
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

}