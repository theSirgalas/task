<?php

use yii\db\Migration;

/**
 * Handles the creation of table `task`.
 */
class m180429_115840_create_tack_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%tack}}', [
            'id' => $this->primaryKey(),
            'name'=>$this->string(255)->notNull(),
            'price'=>$this->integer()->notNull(),
            'data_start'=>$this->dateTime()->notNull(),
            'data_finish'=>$this->dateTime()->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%tack}}');
    }
}
