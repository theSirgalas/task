<?php

use yii\db\Migration;
use app\entities\User;


/**
 * Class m180429_192719_insert_user
 */
class m180429_192719_insert_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert( User::tableName(),[
            'login'=>'admin',
            'first_name'=> 'admin',
            'last_name'=> 'admin',
            'password'=> Yii::$app->security->generatePasswordHash('admin')
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180429_192719_insert_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180429_192719_insert_user cannot be reverted.\n";

        return false;
    }
    */
}
