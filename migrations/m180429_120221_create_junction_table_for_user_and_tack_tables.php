<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_tack`.
 * Has foreign keys to the tables:
 *
 * - `user`
 * - `tack`
 */
class m180429_120221_create_junction_table_for_user_and_tack_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user_tack}}', [
            'user_id' => $this->integer(),
            'tack_id' => $this->integer(),
            'PRIMARY KEY(user_id, tack_id)',
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            'idx-user_tack-user_id',
            'user_tack',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-user_tack-user_id',
            'user_tack',
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `tack_id`
        $this->createIndex(
            'idx-user_tack-tack_id',
            'user_tack',
            'tack_id'
        );

        // add foreign key for table `tack`
        $this->addForeignKey(
            'fk-user_tack-tack_id',
            'user_tack',
            'tack_id',
            '{{%tack}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-user_tack-user_id',
            'user_tack'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'idx-user_tack-user_id',
            'user_tack'
        );

        // drops foreign key for table `tack`
        $this->dropForeignKey(
            'fk-user_tack-tack_id',
            'user_tack'
        );

        // drops index for column `tack_id`
        $this->dropIndex(
            'idx-user_tack-tack_id',
            'user_tack'
        );

        $this->dropTable('{{%user_tack}}');
    }
}
