<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m180429_115830_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'login'=>$this->string(255)->notNull(),
            'first_name'=> $this->string(255)->notNull(),
            'last_name'=>$this->string(255)->notNull(),
            'password'=>$this->string(60)->notNull(),
            'role'=>$this->integer()->defaultValue(0),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user}}');
    }
}
